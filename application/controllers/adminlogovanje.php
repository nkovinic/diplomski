<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminlogovanje extends CI_Controller{
    public function __construct() {
        parent::__construct();    
    }
 
    public function index(){     
        //$korisnik2= $this->session->userdata('korisnik');
        //echo $korisnik2;        
        $this->load->library('form_validation');
        $this->load->helper('form');
        $podaci=array();
        $forma=array(
            'id'=>'formaLog',
            'name'=>'formaLogovanje',
            'method'=>'POST'
        );
        $logEmail=array(
            'id'=>'float-text',
            'name'=>'tbEmail',
            'class'=>'form-control',
            'type'=>'text'
        );
        $logPass=array(
            'id'=>'float-text',
            'name'=>'tbPass',
            'class'=>'form-control',
            'type'=>'password'
        );
        $btnLogovanje=array(             
            'name'=>'btnLogovanje',
            'class'=>'btn btn-blue waves-button waves-light waves-effect',
            'type'=>'submit',
            'content'=>'Logovanje'
        );
        $btnReset=array(               
            'name'=>'btnPonisti',
            'class'=>'btn waves-button waves-effect',
            'content'=>'Ponisti',
            'type'=>'button'
        );

        $podaci['Email']=$logEmail;
        $podaci['Sifra']=$logPass;
        $podaci['Logovanje']=$btnLogovanje;
        $podaci['Ponisti']=$btnReset;
        //print"cao";
        
        $dugme=$this->input->post('btnLogovanje');
        if(isset($dugme)){
            $email=$this->input->post('tbEmail');
            $lozinka=md5($this->input->post('tbPass'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('tbEmail','E-mail','required|trim|valid_email');
            $this->form_validation->set_rules('tbPass','Lozinka','required|min_length[5]');
            $this->form_validation->set_message('required','Polje <b>%s</b> je obavezno!');
            $this->form_validation->set_message('min_length','Minimalna duzina polja <b>%s</b> je 5!');
            $this->form_validation->set_message('valid_email','Unesite polje <b>%s</b> u ispravnom formatu!');
            if($this->form_validation->run()){
                $this->load->model('model_logovanje','korisnik');
                $this->korisnik->email=$email;
                $this->korisnik->password=$lozinka;
                $podaci['korisnik']=$this->korisnik->dohvati();
                if(!empty($podaci['korisnik'])){
                    $korisnik_sesija=$podaci['korisnik']['id_korisnik'];
                    $this->session->set_userdata('id_korisnik',$korisnik_sesija);
                    $uloga=$podaci['korisnik']['naziv_uloga'];
                    $this->session->set_userdata('uloga',$uloga);
                    $ime=$podaci['korisnik']['ime_korisnik'];
                    $this->session->set_userdata('ime',$ime);
                    $prez=$podaci['korisnik']['prezime_korisnik'];
                    $this->session->set_userdata('prezime',$prez);                    
                    $this->session->set_flashdata('ulogovao', 'Uspesno ste se ulogovali <b>'.$ime."</b> !");
                    if($podaci['korisnik']['id_uloga']=='1'){
                        redirect('adminpanel');                     
                    }else{
                        redirect('adminlogovanje');
                    }

                }
            }else{ 
                $this->session->set_flashdata('validacija',  validation_errors());
            }
        }else{
            //redirect('logovanje_registracija/login','refresh');
        }        

        $this->load->view('header');
        $this->load->view('content_logovanje', $podaci);
        $this->load->view('footer');  
    }
    
    public function logout(){
            $korisnik= $this->session->userdata('id_korisnik');
            if(!empty($korisnik)){
                $this->session->unset_userdata('id_korisnik');
                $this->session->sess_destroy();
                redirect('adminlogovanje');
            }
    }
}
