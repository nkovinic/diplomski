<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminpanel extends CI_Controller {
//    public $data;
//    public $izab_kat;
    //adminpanel za pitanja
    
    public function __construct() {
        parent::__construct(); 
        $GLOBALS['izbKat']=FALSE;
    }
    
    public function index($opcija=null,$id=null){
        $formm='adminpanel';
        $this->load->model('model_adminpanel','admin');
        $this->load->library('pagination');
        
        $idKat=$this->input->post('ddlKategorije');
        $this->load->vars($idKat);
        $uloga=$this->session->userdata('uloga');
        $podaci['kategorije']=$this->admin->dohvati_kategorije();
        if($uloga==null || $uloga==""){
            redirect('adminlogovanje');
        }
        $text="";
        $podaci=array();
        $izab_kat=$GLOBALS['izbKat'];
        if($idKat==null || $idKat=='' || $idKat==0 ){              
              $izab_kat=$GLOBALS['izbKat'];
        }else{
             $GLOBALS['izbKat']=$idKat;
             $izab_kat=$idKat;
        }
        echo $izab_kat;
        
        //ako je admin inicijalno prikazivanje, ako je nije filtrirao, sve
        //ako je filtrirao, samo to sto je izabrao
        if($uloga=="Administrator"){
            if($izab_kat==null || $izab_kat=="" || $izab_kat==0){
                $podaci['kategorije']=$this->admin->dohvati_kategorije();
                $podaci['pitanja']=$this->admin->dohvati_pitanja(10);
            }else if($izab_kat!=null || $izab_kat!="" || $izab_kat!=0){
                $this->admin->idKategorija=$izab_kat;
                $podaci['izabranaKategorija']=$izab_kat;
                $podaci['pitanja']=$this->admin->dohvati_pitanja_za_kategoriju(10);
            }
        }
        //ako je je kliknio na obrisi komunikacija sa bazom i brisanje
        if($opcija=="obrisi" && $id!=null && $uloga="Administrator"){
            $this->admin->idPitanje=$id;
            $podaci['korisnik']=$this->admin->obrisi_pitanje();
            redirect('adminpanel');
        }
        //ako je kliknuo na izmeni, otvara se forma za izmenu
        if($opcija=="izmeni" && $id!=null && $uloga="Administrator"){
            $this->admin->idPitanje=$id;
            $podaci['kategorije']=$this->admin->dohvati_kategorije();//DOHVATA SVE KATEGORIJE
            $podaci['kategorijaPitanje']=$this->admin->dohvati_kategorije_za_pitanje();//DOHVATA KATEGORIJE ZA DATO PITANJE
            $podaci['oblasti']=$this->admin->dohvati_oblasti();//DOHVATA SVE OBLASTI
            $podaci['oblastPitanje']=$this->admin->dohvati_oblast_za_pitanje();//DOHVATA oblast ZA DATO PITANJE
            $podaci['odgovor']=$this->admin->odgovori_za_pitanje();//DOHVATA ODGOVORE ZA DATO PITANJE
            $podaci['pitanje']=$this->admin->dohvati_pitanje();//DOHVATA PITANJE KOJE SE TRAZI
            $this->session->set_userdata('tmp_id_pitanje',$id);            
        }
        $btnSub=array(
            'name'=>'btnIzmena',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Izmeni'
        );
        $lbIzmena=array(
            'class'=>'floating-label',
            'name'=>'lbIzmena',
            'id'=>'lbIzmena'
        );
        $tbIzmena=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'text',
            'name'=>'tbIzmena',
            'value'=>$text
        );
        //paginacija pitanja
        //$limit=15;
        $brojPitanja=$this->admin->brojPitanja();
        $config['total_rows'] = $brojPitanja;
        $config['per_page'] = 7;
        $config['uri_segment']= 3;
        //$config['attributes']['rel'] = FALSE;
        $config['base_url'] =base_url()."adminpanel/index/";//site_url('adminpanel');
        $config['full_tag_open'] = "<nav align='center'><ul class='pagination'>";
        $config['full_tag_close'] ="</nav></ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $this->pagination->initialize($config); 
        $podaci['pagination_linkovi']=$this->pagination->create_links();      
        
        $dugme=$this->input->post('btnIzmena');
        $dugme2=$this->input->post('btnDodaj');
        if(isset($dugme) && $dugme=$this->input->post('btnIzmena')== 'btnIzmena'){
          $idP=$this->session->userdata('tmp_id_pitanje');
          $textPitanje=$this->input->post('taTekstPitanja');//tb gde je za unos pitanja
          $oblastPitanje=$this->input->post('oblast');
          $kat1P=$this->input->post('katA');
          $kat2P=$this->input->post('katB');
          $kat3P=$this->input->post('katC');
          $kat4P=$this->input->post('katD');
          $kat5P=$this->input->post('katF');
          $odg1P=$this->input->post('taOdgovor1');
          $odg2P=$this->input->post('taOdgovor2');
          $odg3P=$this->input->post('taOdgovor3');
          $odg4P=$this->input->post('taOdgovor4');
          $odg5P=$this->input->post('taOdgovor5');
          $odg6P=$this->input->post('taOdgovor6');
          $odg1TP=$this->input->post('tacan_odgovor_1');
          $odg2TP=$this->input->post('tacan_odgovor_2');
          $odg3TP=$this->input->post('tacan_odgovor_3');
          $odg4TP=$this->input->post('tacan_odgovor_4');
          $odg5TP=$this->input->post('tacan_odgovor_5');
          $odg6TP=$this->input->post('tacan_odgovor_6');
          $slikaPitanje=$this->input->post('slika');
          $brBodovaP=$this->input->post('bodovi');
          $viseOdgovora=$this->input->post('tacan');
          if($textPitanje!=""){  
            $this->admin->textPitanje=$textPitanje;
            $this->admin->idPitanje=$idP;
            $this->admin->oblastPitanje=$oblastPitanje;
            $this->admin->kat1P=$kat1P;
            $this->admin->kat2P=$kat2P;
            $this->admin->kat3P=$kat3P;
            $this->admin->kat4P=$kat4P;
            $this->admin->kat5P=$kat5P;
            $this->admin->odg1P=$odg1P;
            $this->admin->odg2P=$odg2P;
            $this->admin->odg3P=$odg3P;
            $this->admin->odg4P=$odg4P;
            $this->admin->odg5P=$odg5P;
            $this->admin->odg6P=$odg6P;
            $this->admin->$odg1TP=$odg1TP;
            $this->admin->$odg2TP=$odg2TP;
            $this->admin->$odg3TP=$odg3TP;
            $this->admin->$odg4TP=$odg4TP;
            $this->admin->$odg5TP=$odg5TP;
            $this->admin->$odg6TP=$odg6TP;
            $this->admin->slikaPitanje=$slikaPitanje;
            $this->admin->brBodovaP=$brBodovaP;
            $this->admin->viseOdgovora=$viseOdgovora;
            
            $this->admin->izmeni_pitanje();
             redirect('adminpanel/');
          }
        }
        if(isset($dugme2) && $this->input->post('btnDodaj')== 'btnDodaj'){
          $idP=$this->session->userdata('tmp_id_pitanje');
          $textPitanje=$this->input->post('taTekstPitanja');//tb gde je za unos pitanja
          $oblastPitanje=$this->input->post('oblast');
          $kat1P=$this->input->post('katA');
          $kat2P=$this->input->post('katB');
          $kat3P=$this->input->post('katC');
          $kat4P=$this->input->post('katD');
          $kat5P=$this->input->post('katF');
          $odg1P=$this->input->post('taOdgovor1');
          $odg2P=$this->input->post('taOdgovor2');
          $odg3P=$this->input->post('taOdgovor3');
          $odg4P=$this->input->post('taOdgovor4');
          $odg5P=$this->input->post('taOdgovor5');
          $odg6P=$this->input->post('taOdgovor6');
          $odg1TP=$this->input->post('tacan_odgovor_1');
          $odg2TP=$this->input->post('tacan_odgovor_2');
          $odg3TP=$this->input->post('tacan_odgovor_3');
          $odg4TP=$this->input->post('tacan_odgovor_4');
          $odg5TP=$this->input->post('tacan_odgovor_5');
          $odg6TP=$this->input->post('tacan_odgovor_6');
          $slikaPitanje=$this->input->post('slika');
          $brBodovaP=$this->input->post('bodovi');
          $viseOdgovora=$this->input->post('tacan');
          if($text_za_izmenu!=""){  
            $this->admin->textPitanje=$textPitanje;
            $this->admin->idPitanje=$idP;
            $this->admin->oblastPitanje=$oblastPitanje;
            $this->admin->kat1P=$kat1P;
            $this->admin->kat2P=$kat2P;
            $this->admin->kat3P=$kat3P;
            $this->admin->kat4P=$kat4P;
            $this->admin->kat5P=$kat5P;
            $this->admin->odg1P=$odg1P;
            $this->admin->odg2P=$odg2P;
            $this->admin->odg3P=$odg3P;
            $this->admin->odg4P=$odg4P;
            $this->admin->odg5P=$odg5P;
            $this->admin->odg6P=$odg6P;
            $this->admin->$odg1TP=$odg1TP;
            $this->admin->$odg2TP=$odg2TP;
            $this->admin->$odg3TP=$odg3TP;
            $this->admin->$odg4TP=$odg4TP;
            $this->admin->$odg5TP=$odg5TP;
            $this->admin->$odg6TP=$odg6TP;
            $this->admin->slikaPitanje=$slikaPitanje;
            $this->admin->brBodovaP=$brBodovaP;
            $this->admin->viseOdgovora=$viseOdgovora;
            
            $this->admin->dodaj_pitanje();
            redirect('adminpanel/');
          }
        }
        
        
        $podaci['lbIzmen']=$lbIzmena;
        $podaci['taIzmena']=$tbIzmena;
        $podaci['btnIzmena']=$btnSub;
        $podaci['formm']=$formm;
        
        if($opcija=='izmeni' && $uloga="Administrator"){
          $this->load->view('header_admin');
          $this->load->view('content_admin_pitanje', $podaci);
          $this->load->view('footer');  
        }else{
          $this->load->view('header_admin');
          $this->load->view('content_admin', $podaci);
          $this->load->view('footer');  
        }
    }
    
    //adminpanel za oblasti
    public function Oblasti($opcija=null,$id=null){
        $formm='adminpanel/Oblasti/';
        $this->load->model('model_adminpanel','admin');
        $podaci['oblasti']=$this->admin->dohvati_oblasti();
        $uloga=$this->session->userdata('uloga');
        if($uloga==null || $uloga==""){
            redirect('adminlogovanje');
        }
        $podaci=array();
        $text="";
        
        //ako je kliknuo na izmeni, komunikacija sa bazom i izlvacenje pod
        if($uloga="Administrator"){
            $podaci['oblasti']=$this->admin->dohvati_oblasti();
        }
        if($opcija=="obrisi" && $id!=null && $uloga="Administrator"){
            $this->admin->idOblasti=$id;
            $this->admin->obrisi_oblast();
            redirect('adminpanel/Oblasti/');
        }
        if($opcija=="izmeni" && $id!=null && $uloga="Administrator"){
            $this->admin->idOblasti=$id;
            $podaci['oblast']=$this->admin->dohvati_obl();     
            $podaci['oblasti']=$this->admin->dohvati_oblasti();
            $text=$podaci['oblast']['oblast_text'];
            $this->session->set_userdata('tmp_id_oblasti',$id);
        }
        //na dugme izmeni ili dodaj
        $dugme=$this->input->post('btnIzmena');
        $dugme2=$this->input->post('btnDodaj');
        if(isset($dugme) && $dugme=$this->input->post('btnIzmena')== 'btnIzmena'){
          $id=$this->session->userdata('tmp_id_oblasti');
          $text_za_izmenu=$this->input->post('tbIzmena');
          if($text_za_izmenu!=""){  
            $this->admin->oblastText=$text_za_izmenu;
            $this->admin->idOblasti=$id;
            $this->admin->izmeni_oblast();
            $this->session->unset_userdata('tmp_id_oblasti');
            redirect('adminpanel/Oblasti/');
          }
        }
        if(isset($dugme2) && $this->input->post('btnDodaj')== 'btnDodaj'){
          $text_za_izmenu=$this->input->post('tbIzmena');
          echo $text_za_izmenu;
          if($text_za_izmenu!=""){  
            $this->admin->oblastText=$text_za_izmenu;
            $this->admin->dodaj_oblast();
            redirect('adminpanel/Oblasti/');
          }
        }
        //kreiranje forme       
        $btnSub=array(
            'name'=>'btnIzmena',
            'value'=>'btnIzmena',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Izmeni'
        );
        $btnSub2=array(
            'name'=>'btnDodaj',
            'value'=>'btnDodaj',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Dodaj oblast'
        );
        $lbIzmena=array(
            'class'=>'floating-label',
            'name'=>'lbIzmena',
            'id'=>'lbIzmena'
        );
        $tbIzmena=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'text',
            'name'=>'tbIzmena',
            'value'=>$text
        );
        //podaci za prosledjivanje
        $podaci['lbIzmen']=$lbIzmena;
        $podaci['taIzmena']=$tbIzmena;
        $podaci['btnIzmena']=$btnSub;
        $podaci['btnDodaj']=$btnSub2;
        $podaci['formm']=$formm;
        //view koji se pozivaju 
        $this->load->view('header_admin');
        $this->load->view('content_admin_oblasti', $podaci);
        $this->load->view('footer');  

    }     
    
    //adminpanel za aktivnosti
    public function  Aktivnosti($opcija=null,$id=null){
        $formm='adminpanel/aktivnosti/';
        $this->load->model('model_adminpanel','admin');
        $this->load->library('pagination');
        $uloga=$this->session->userdata('uloga');
        $podaci['aktivnosti']=$this->admin->dohvati_aktivnosti();
        if($opcija=="obrisi" && $id!=null && $uloga="Administrator"){
            $this->admin->idAktivnost=$id;
            $this->admin->obrisi_aktivnost();
            redirect('adminpanel/Aktivnosti/');
        }
        
        //kreiranje forme       
        $btnSub=array(
            'name'=>'btnIzmena',
            'value'=>'btnIzmena',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Izmeni'
        );
        $lbIzmena=array(
            'class'=>'floating-label',
            'name'=>'lbIzmena',
            'id'=>'lbIzmena'
        );
        $tbIzmena=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'text',
            'name'=>'tbIzmena',
        );
        
        //podaci za prosledjivanje
        $podaci['lbIzmen']=$lbIzmena;
        $podaci['taIzmena']=$tbIzmena;
        $podaci['btnIzmena']=$btnSub;
        $podaci['formm']=$formm;
        $this->load->view('header_admin');
        $this->load->view('content_admin_aktivnost', $podaci);
        $this->load->view('footer');
    }
    
    //adminpanel za pretragu
    public function Pretraga($opcija=null,$id=null){
        $formm='adminpanel/Pretraga/';
        $this->load->model('model_adminpanel','admin');
        $uloga=$this->session->userdata('uloga');
        if($uloga==null || $uloga==""){
            redirect('adminlogovanje');
        }
        $podaci=array();
        $text="";
        
        //ako je kliknuo na izmeni, komunikacija sa bazom i izlvacenje pod
        if($uloga="Administrator"){

             $data['pitanja']=$this->admin->dohvati_pitanja_bez_paginacije();

             $podaci['pitanja']=$this->admin->dohvati_pitanja_bez_paginacije();

        }
        if($opcija=="obrisi" && $id!=null && $uloga="Administrator"){
            //kod za brisanje pitanja
            $this->admin->idPitanje=$id;
            $podaci['korisnik']=$this->admin->obrisi_pitanje();
            redirect('adminpanel/Pretraga');
        }
        if($opcija=="izmeni" && $id!=null && $uloga="Administrator"){
            //kod za izmenu pitanja
            $this->admin->idPitanje=$id;
            $podaci['kategorije']=$this->admin->dohvati_kategorije();     
            $podaci['oblasti']=$this->admin->dohvati_oblasti();
            $podaci['odgovor']=$this->admin->odgovori_za_pitanje();
            $podaci['pitanje']=$this->admin->dohvati_pitanje();
            $this->session->set_userdata('tmp_id_pitanje',$id);
        }
        //na dugme izmeni ili dodaj
        $dugme=$this->input->post('btnIzmena');
        $dugme2=$this->input->post('btnDodaj');
        if(isset($dugme) && $dugme=$this->input->post('btnIzmena')== 'btnIzmena'){
          $id=$this->session->userdata('tmp_id_oblasti');
          $text_za_izmenu=$this->input->post('tbIzmena');
          if($text_za_izmenu!=""){  
            $this->admin->oblastText=$text_za_izmenu;
            $this->admin->idOblasti=$id;
            $this->admin->izmeni_oblast();
            $this->session->unset_userdata('tmp_id_oblasti');
            redirect('adminpanel/Oblasti/');
          }
        }
        if(isset($dugme2) && $this->input->post('btnDodaj')== 'btnDodaj'){
          $text_za_izmenu=$this->input->post('tbIzmena');
          echo $text_za_izmenu;
          if($text_za_izmenu!=""){  
            $this->admin->oblastText=$text_za_izmenu;
            $this->admin->dodaj_oblast();
            redirect('adminpanel/Oblasti/');
          }
        }
        //kreiranje forme       
//        $btnSub=array(
//            'name'=>'btnIzmena',
//            'value'=>'btnIzmena',
//            'class'=>'btn btn-alt waves-button waves-effect waves-light',
//            'type'=>'submit',
//            'content'=>'Izmeni'
//        );
//        $btnSub2=array(
//            'name'=>'btnDodaj',
//            'value'=>'btnDodaj',
//            'class'=>'btn btn-alt waves-button waves-effect waves-light',
//            'type'=>'submit',
//            'content'=>'Dodaj oblast'
//        );
//        $lbIzmena=array(
//            'class'=>'floating-label',
//            'name'=>'lbIzmena',
//            'id'=>'lbIzmena'
//        );
//        $tbIzmena=array(
//            'class'=>'form-control textarea-autosize',
//            'id'=>'float-textarea-autosize',
//            'rows'=>'1',
//            'type'=>'text',
//            'name'=>'tbIzmena',
//            'value'=>$text
//        );
        //podaci za prosledjivanje
        
       
        //view koji se pozivaju 
        $this->load->view('header_admin');
        $this->load->view('content_admin_pretraga');
        $this->load->view('footer');  

    }     
    public function JSON(){
        $this->load->model('model_adminpanel','admin');
        $json['pitanja']=$this->admin->dohvati_pitanja_bez_paginacije();
        echo json_encode($json);
        
    }
    public function JSON_aktivnosti(){
        $this->load->model('model_adminpanel','admin');
        $json['pitanja']=$this->admin->dohvati_aktivnosti();
        echo json_encode($json);
        
    }
}



