<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Start extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('model_pitanja', 'mp');
    }
    public function index(){
            $data['kategorije'] = $this->mp->dohvati_kategorije();
			$this->load->view('header');
            $this->load->view('izaberi', $data);
            $this->load->view('footer');
        }
    
	public function pitanja($option)
	{
            
            $data['greske']=array();
            $data=array();
            $data['pitanja']=array();
            $data['odgovori']=array();
            
            
            
                
                if($option=="1" || $option=="2" || $option=="6"){
                   $data['pitanja'] = $this->mp->dohvati_pitanja_ABF();//za C i D kategoriju je funkcija dohvati_pitanja_CD()
                }else if($option=="3" || $option=="4"){
                   $data['pitanja'] = $this->mp->dohvati_pitanja_CD();//za C i D kategoriju je funkcija dohvati_pitanja_CD()
                }else if($option=="0"){
                   //echo "<script>alert('Niste nista izabrali, izaberite kategoriju!');</script>";
                   $url="http://localhost/testovi_pitanja/";//ovde raditi sa base_url();
                   redirect($uri);
                }           
            
            
            //$data['pitanja'] = $this->mp->dohvati_pitanja_ABF();//za C i D kategoriju je funkcija dohvati_pitanja_CD()
           foreach ($data['pitanja'] as $p) {
                $this->mp->idPitanje=$p['id_pitanje'];
                //echo $p['id_pitanje'].',';
                $idP=$p['id_pitanje'];
                $data['odgovori'][$idP]=$this->mp->odgovori_za_pitanje();
                
                
            }
			echo json_encode($data);
            
            //$data['kategorije'] = $this->mp->dohvati_kategorije();
            //print_r($data['odgovori']);
            //$data['odgovori'] = $this->mp->odgovori_za_pitanje();
            //$data['pitanja']=$this->mp->dohvati_pitanja();
           // $this->load->view('header');
            //$this->load->view('content', $data);
            //$this->load->view('footer');
	}
	
	public function kategorija(){
            $dugme=$this->input->post('btnStart');
            if(isset($dugme)){
            $data['kat'] = $this->input->post('ddlKategorije');
            
            
            $this->load->view('header');
            $this->load->view('servis_content', $data);
            $this->load->view('footer');            

			
        }
    }
        public function result($brBodova, $ukupnoBodova){
            $rezProcenti = $brBodova/$ukupnoBodova * 100;
            $data['rezultati'] = array(
                'brojBodova' => $brBodova,
                'ukupnoBodova' => $ukupnoBodova,
                'procenti' => $rezProcenti
            );
            $this->load->view('header');
            $this->load->view('prosaoPao', $data);            
            $this->load->view('footer');
        }
}

