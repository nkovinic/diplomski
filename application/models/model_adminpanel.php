<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_adminpanel
 *
 * @author kovin
 */
class model_adminpanel extends CI_Model{
    //za pitanja
    public $idPitanje;//dohvatanje id pitanja
    public $textPitanje;//za text pitanja
    public $oblastPitanje;//
    public $kat1P;
    public $kat2P;
    public $kat3P;
    public $kat4P;
    public $kat5P;
    public $odg1P;
    public $odg1TP;
    public $odg2P;
    public $odg2TP;
    public $odg3P;
    public $odg3TP;
    public $odg4P;
    public $odg4TP;
    public $odg5P;
    public $odg5TP;
    public $odg6P;
    public $odg6TP;
    public $slikaPitanje;
    public $brBodovaP;
    public $viseOdgovora;
    public $idUnet;


    //kategorije
    public $idKategorija;
    
    //oblasti
    public $idOblasti;
    public $oblastText;
    
    //aktivnosti
    public $idAktivnost;
    public $emailAktivnost;
    public $poeniAktivnost;


    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    //funkcije za pitanja
    public function dohvati_pitanja($limit = 6){
        $offset=$this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('pitanja');
        $this->db->join('pitanja_oblasti', 'pitanja.id_pitanje = pitanja_oblasti.id_pitanje');
        $this->db->join('pitanja_kategorije', 'pitanja.id_pitanje = pitanja_kategorije.id_pitanje');
        $this->db->join('kategorije', 'kategorije.id_kategorija = pitanja_kategorije.id_kategorija');
        $this->db->join('oblasti', 'oblasti.id_oblasti = pitanja_oblasti.id_oblasti');
        $this->db->limit($limit,$offset);
        return $this->db->get()->result_array();
    }
    public function dohvati_pitanja_bez_paginacije(){
        $this->db->select('*');
        $this->db->from('pitanja');
//        $this->db->join('pitanja_oblasti', 'pitanja.id_pitanje = pitanja_oblasti.id_pitanje');
//        $this->db->join('pitanja_kategorije', 'pitanja.id_pitanje = pitanja_kategorije.id_pitanje');
//        $this->db->join('kategorije', 'kategorije.id_kategorija = pitanja_kategorije.id_kategorija');
//        $this->db->join('oblasti', 'oblasti.id_oblasti = pitanja_oblasti.id_oblasti');
        return $this->db->get()->result_array();
    }  
    public function dohvati_pitanja_za_oblast($limit = 6){   
        $offset=$this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('pitanja');
        $this->db->join('pitanja_oblasti', 'pitanja.id_pitanje = pitanja_oblasti.id_pitanje');
        $this->db->join('oblasti', 'oblasti.id_oblasti = pitanja_oblasti.id_oblasti');
        $this->db->where('oblasti.id_oblasti',$this->idOblasti);
        $this->db->limit($limit,$offset);
        return $this->db->get()->result_array();
    }
    public function dohvati_pitanja_za_kategoriju($limit = 6){   
        $offset=$this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('pitanja');
        $this->db->join('pitanja_kategorije', 'pitanja.id_pitanje = pitanja_kategorije.id_pitanje');
        $this->db->join('kategorije', 'kategorije.id_kategorija = pitanja_kategorije.id_kategorija');
        $this->db->where('kategorije.id_kategorija',$this->idKategorija);
        $this->db->limit($limit,$offset);
        return $this->db->get()->result_array();
    }
    public function dohvati_pitanje(){  
        $this->db->select('*');
        $this->db->from('pitanja');
        $this->db->join('pitanja_oblasti', 'pitanja.id_pitanje = pitanja_oblasti.id_pitanje');
        $this->db->join('oblasti', 'oblasti.id_oblasti = pitanja_oblasti.id_oblasti');
        $this->db->where('pitanja.id_pitanje',$this->idPitanje);
        return $this->db->get()->result_array();
    }
    public function obrisi_pitanje(){
        $this->db->where('id_pitanje', $this->idPitanje);
        $this->db->delete('pitanja');
    }
    public function izmeni_pitanje(){
        //pitanje
        $niz=array(
            "pitanje_text"=>  $this->textPitanje,
            "pitanje_slika"=>  $this->slikaPitanje,
            "pitanje_vrsta"=>  $this->viseOdgovora,
            "pitanje_poeni"=>  $this->brBodovaP
        );
        $this->db->where('id_pitanje', $this->idPitanje);
        $this->db->update('pitanja', $niz);
        //kategorije
        if(($this->kat1P)!="" || ($this->kat1P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idPitanje,
            "id_kategorija"=>  $this->kat1P
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 1);
            $this->db->update('pitanja_kategorije', $niz1);
        }else{
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 1);
            $this->db->delete('pitanja_kategorije');
        }
        if(($this->kat2P)!="" || ($this->kat2P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idPitanje,
            "id_kategorija"=>  $this->kat2P
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 2);
            $this->db->update('pitanja_kategorije', $niz1);
        }else{
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 2);
            $this->db->delete('pitanja_kategorije');
        }
        if(($this->kat3P)!="" || ($this->kat3P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idPitanje,
            "id_kategorija"=>  $this->kat3P
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 3);
            $this->db->update('pitanja_kategorije', $niz1);
        }else{
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 3);
            $this->db->delete('pitanja_kategorije');
        }
        if(($this->kat4P)!="" || ($this->kat4P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idPitanje,
            "id_kategorija"=>  $this->kat4P
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 4);
            $this->db->update('pitanja_kategorije', $niz1);
        }else{
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 4);
            $this->db->delete('pitanja_kategorije');
        }
        if(($this->kat5P)!="" || ($this->kat5P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idPitanje,
            "id_kategorija"=>  $this->kat5P
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 5);
            $this->db->update('pitanja_kategorije', $niz1);
        }else{
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->where('id_kategorija', 5);
            $this->db->delete('pitanja_kategorije');
        }
        
        //odgovori
        if(($this->odg1P)!="" || ($this->odg1P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg1P,
            "odgovor_tacan"=>  $this->odg1TP
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }else{
            $niz1=array(
            "odgovor_text"=>  $this->odg1P,
            "odgovor_tacan"=>  0
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }
        if(($this->odg2P)!="" || ($this->odg2P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg2P,
            "odgovor_tacan"=>  $this->odg2TP
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }else{
            $niz1=array(
            "odgovor_text"=>  $this->odg2P,
            "odgovor_tacan"=>  0
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }
        if(($this->odg3P)!="" || ($this->odg3P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg3P,
            "odgovor_tacan"=>  $this->odg3TP
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }else{
            $niz1=array(
            "odgovor_text"=>  $this->odg3P,
            "odgovor_tacan"=>  0
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }
        if(($this->odg4P)!="" || ($this->odg4P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg4P,
            "odgovor_tacan"=>  $this->odg4TP
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }else{
            $niz1=array(
            "odgovor_text"=>  $this->odg4P,
            "odgovor_tacan"=>  0
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }
        if(($this->odg5P)!="" || ($this->odg5P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg5P,
            "odgovor_tacan"=>  $this->odg5TP
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }else{
            $niz1=array(
            "odgovor_text"=>  $this->odg5P,
            "odgovor_tacan"=>  0
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }
        if(($this->odg6P)!="" || ($this->odg6P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg6P,
            "odgovor_tacan"=>  $this->odg6TP
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }else{
            $niz1=array(
            "odgovor_text"=>  $this->odg6P,
            "odgovor_tacan"=>  0
            );
            $this->db->where('id_pitanje', $this->idPitanje);
            $this->db->update('odgovori', $niz1);
        }
           
        //oblast
        $niz2=array(
            "id_pitanje"=>  $this->idPitanje,
            "id_oblast"=>  $this->oblastPitanje
        );
        $this->db->where('id_pitanje', $this->idPitanje);
        $this->db->update('pitanja_oblasti', $niz2);
        
    }
    public function dodaj_pitanje(){
        $niz=array(
            "pitanje_text"=>  $this->textPitanje,
            "pitanje_slika"=>  $this->slikaPitanje,
            "pitanje_vrsta"=>  $this->viseOdgovora,
            "pitanje_poeni"=>  $this->brBodovaP
        );
        $this->db->insert('pitanja', $niz);
        $this->idUnet=$this->db->insert_id();
        //kategorije
        if(($this->kat1P)!="" || ($this->kat1P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idUnet,
            "id_kategorija"=>  $this->kat1P
            );
            $this->db->insert('pitanja_kategorije', $niz1);
        }
        if(($this->kat2P)!="" || ($this->kat2P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idUnet,
            "id_kategorija"=>  $this->kat2P
            );
            $this->db->insert('pitanja_kategorije', $niz1);
        }
        if(($this->kat3P)!="" || ($this->kat3P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idUnet,
            "id_kategorija"=>  $this->kat3P
            );
            $this->db->insert('pitanja_kategorije', $niz1);
        }
        if(($this->kat4P)!="" || ($this->kat4P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idUnet,
            "id_kategorija"=>  $this->kat4P
            );
            $this->db->insert('pitanja_kategorije', $niz1);
        }
        if(($this->kat5P)!="" || ($this->kat5P)!=null){
            $niz1=array(
            "id_pitanje"=>  $this->idUnet,
            "id_kategorija"=>  $this->kat5P
            );
           $this->db->insert('pitanja_kategorije', $niz1);
        }
        
        //odgovori
        if(($this->odg1P)!="" || ($this->odg1P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg1P,
            "odgovor_tacan"=>  $this->odg1TP,
            "id_pitanje"=>  $this->idUnet
            );
            $this->db->insert('odgovori', $niz1);
        }
        if(($this->odg2P)!="" || ($this->odg2P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg2P,
            "odgovor_tacan"=>  $this->odg2TP,
            "id_pitanje"=>  $this->idUnet
            );
            $this->db->insert('odgovori', $niz1);
        }
        if(($this->odg3P)!="" || ($this->odg3P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg3P,
            "odgovor_tacan"=>  $this->odg3TP,
            "id_pitanje"=>  $this->idUnet
            );
            $this->db->insert('odgovori', $niz1);
        }
        if(($this->odg4P)!="" || ($this->odg4P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg4P,
            "odgovor_tacan"=>  $this->odg4TP,
            "id_pitanje"=>  $this->idUnet
            );
            $this->db->insert('odgovori', $niz1);
        }
        if(($this->odg5P)!="" || ($this->odg5P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg5P,
            "odgovor_tacan"=>  $this->odg5TP,
            "id_pitanje"=>  $this->idUnet
            );
            $this->db->insert('odgovori', $niz1);
        }
        if(($this->odg6P)!="" || ($this->odg6P)!=null){
            $niz1=array(
            "odgovor_text"=>  $this->odg6P,
            "odgovor_tacan"=>  $this->odg6TP,
            "id_pitanje"=>  $this->idUnet
            );
            $this->db->insert('odgovori', $niz1);
        }
        
           
        //oblast
        $niz2=array(
            "id_pitanje"=>  $this->idUnet,
            "id_oblast"=>  $this->oblastPitanje
        );
        $this->db->insert('pitanja_oblasti', $niz2);
    }//za paginaciju
    public function brojPitanja(){
        return $this->db->count_all_results('pitanja');
    }
    public function pagination_pitanje($limit = 6){
        $offset=$this->uri->segment(3);
        $this->db->limit($limit,$offset); 
        return $this->db->get_where('pitanja', array('aktivan_post'=>1))->result_array();
    }
    
    //funkcije za kategorije
    public function dohvati_kategorije(){
        $this->db->select('*');
        $this->db->from('kategorije');
        return $this->db->get()->result_array();
    }
    public function dohvati_kategorije_za_pitanje(){
        $this->db->select('*');
        $this->db->from('kategorije');
        $this->db->join('pitanja_kategorije', 'pitanja_kategorije.id_kategorija = pitanja_kategorije.id_kategorija');
        $this->db->where('pitanja_kategorije.id_pitanje',$this->idPitanje);
        return $this->db->get()->result_array();
    }
    
    //funkcije za odgovore
    public function odgovori_za_pitanje(){
        $this->db->select('*');
        $this->db->from('odgovori');
        $this->db->where('id_pitanje', $this->idPitanje);
        return $this->db->get()->result_array();
    }
    
    //funkcije za oblasti
    public function dohvati_oblasti(){
        $this->db->select('*');
        $this->db->from('oblasti');
        return $this->db->get()->result_array();
    }
    public function dohvati_obl(){
        $this->db->select('*');
        $this->db->from('oblasti');
        $this->db->where('id_oblasti', $this->idOblasti);
        return $this->db->get()->row_array();//za hvatanje jedne oblasti
    }
    public function dohvati_oblast_za_pitanje(){
        $this->db->select('*');
        $this->db->from('oblasti');
        $this->db->join('pitanja_oblasti', 'oblasti.id_oblasti = pitanja_oblasti.id_oblasti');
        $this->db->where('id_pitanje', $this->idPitanje);
        return $this->db->get()->row_array();//za hvatanje jedne oblasti
    }
    
    public function izmeni_oblast(){
        $podaci = array('oblast_text' => $this->oblastText);
        $this->db->where('id_oblasti', $this->idOblasti);
        $this->db->update('oblasti', $podaci);
    }
    public function obrisi_oblast(){
        $this->db->where('id_oblasti', $this->idOblasti);
        $this->db->delete('oblasti');
    }
    public function dodaj_oblast(){
        $podaci = array('oblast_text' => $this->oblastText);
        $this->db->insert('oblasti', $podaci); 
    }
    
    //funkcije za aktivnosti
    public  function dohvati_aktivnosti(){
        $this->db->select('*');
        $this->db->from('aktivnosti');
        return $this->db->get()->result_array();
    }
    public function brojAktivnosti(){
        return $this->db->count_all_results('aktivnosti');
    }
    public function obrisi_aktivnost(){
        $this->db->where('id_aktivnost', $this->idAktivnost);
        $this->db->delete('aktivnosti');
    }
    public function dodaj_aktivnost(){
        $date=time();
        $podaci = array(
            'email_aktivnost' => $this->emailAktivnost,
            'poeni_aktivnost' => $this->poeniAktivnost,
            'datum_aktivnost' => $date
            );
        $this->db->insert('aktivnosti',$podaci);
    }
    
}
