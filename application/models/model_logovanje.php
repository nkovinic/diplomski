<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_logovanje
 *
 * @author kovin
 */
class Model_logovanje extends CI_Model{
    public $idkorisnik;
    public $ime;
    public $prezime;
    public $email;
    public $username;
    public $password;    
    public $uloga;
    public $aktivan;
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    //za logovanje
    function dohvati(){
            $uslov=array("mail_korisnik"=>$this->email,"pass_korisnik"=>$this->password);
            $this->db->select('*');
            $this->db->from('korisnici');
            $this->db->join('uloga','uloga.id_uloga=korisnici.id_uloga');
            $this->db->where($uslov);
            $query=$this->db->get(); 
            return $query->row_array();
    }
    
    
}
