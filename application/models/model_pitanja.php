<?php


class model_pitanja  extends CI_Model {
    
    public $idPitanje;
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function dohvati_pitanja_ABF(){
        $q="(SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=1 ORDER BY RAND() LIMIT 18) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=2 ORDER BY RAND() LIMIT 14) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=3 ORDER BY RAND() LIMIT 4) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=4 ORDER BY RAND() LIMIT 3) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=5 ORDER BY RAND() LIMIT 2) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=6 ORDER BY RAND() LIMIT 1)";
        $query = $this->db->query($q);
        return $query->result_array();
    }
    public function dohvati_pitanja_CD(){
        $q="(SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=1 ORDER BY RAND() LIMIT 18) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=2 ORDER BY RAND() LIMIT 14) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=3 ORDER BY RAND() LIMIT 5) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=4 ORDER BY RAND() LIMIT 8) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=5 ORDER BY RAND() LIMIT 2) UNION
            (SELECT * FROM pitanja p JOIN pitanja_oblasti po ON p.id_pitanje=po.id_pitanje JOIN oblasti o ON o.id_oblasti=po.id_oblasti WHERE o.id_oblasti=6 ORDER BY RAND() LIMIT 1)";
        $query = $this->db->query($q);
        return $query->result_array();
    }
    public function odgovori_za_pitanje(){
        $this->db->select('*');
        $this->db->from('odgovori');
        $this->db->where('id_pitanje', $this->idPitanje);
        return $this->db->get()->result_array();        
    }
    /*
    public function dohvati_pitanja(){ 
        $pod   =   array();
        //$id = $this->uri->segment(3, 0);
        $this->db->select('*');
        $this->db->from('pitanja');
        $this->db->join('pitanja_oblasti','pitanja_oblasti.id_pitanje=pitanja.id_pitanje');
        $this->db->join('oblasti','oblasti.id_oblasti=pitanja_oblasti.id_oblasti');
        $this->db->where('oblasti.id_oblasti',4);
        $this->db->order_by('pitanja.id_pitanje','random');
        $this->db->limit(10);
        $q = $this->db->get(); 
        foreach ($q->result_array() as $row){
            $pod['id_pitanje'] = $row['id_pitanje'];
            $pod['pitanje_text'] = $row['pitanje_text'];
            $pod['pitanje_slika'] = $row['pitanje_slika'];
            $this->idPitanje=$row['id_pitanje'];
            $this->db->select('*');
            $this->db->from('odgovori');
            $this->db->where('id_pitanje', $this->idPitanje);
            $pod['odgovori']=$this->db->get()->result_array(); 
        }
    return $pod;
    }*/
    
    public function dohvati_kategorije(){
        $this->db->select('*');
        $this->db->from('kategorije');
        return $this->db->get()->result();
    }
    
}
//Ovo je upit koji vadi random pitanja 
//SELECT * FROM pitanja p JOIN odgovori o ON p.id_pitanje=o.id_pitanje JOIN pitanja_oblasti po ON po.id_pitanje=p.id_pitanje JOIN oblasti ob ON ob.id_oblasti=po.id_oblasti WHERE ob.id_oblasti=4 ORDER BY RAND() LIMIT 10