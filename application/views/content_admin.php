<div class="container">
    <div class="col-lg-6 col-md-12 col-sm-8">
        <?php print form_open('adminpanel/')?>
            <h3>Изаберите жељену категорију</h3>
            <select name="ddlKategorije" id="ddlKategorije">
                <option value="0">Изабери...</option>
                <?php foreach($kategorije as $k):?>
                <option value="<?php print $k['id_kategorija'] ?>"><?php print $k['kategorija']?></option>
                <?php endforeach; ?>
            </select><br/>
            <button class="btnStart" id="btnStart" name="btnStart">Филтрирај</button>
            <button class="btnStart" id="btnStart" name="btnStart">Додај питање</button>
            
        <?php print form_close();?>
    </div>
    <div class="col-md-3 content-fix">
        <div class="content-fix-scroll">
            <div class="content-fix-wrap">
                <div class="content-fix-inner">
                    <?php echo $pagination_linkovi; ?>
                </div>
        </div>
        </div>
    </div>
    <table class="table table-hover table-stripe" title="Tabela članova">
        <thead>
            <tr>
                    <th>Текст питања</th>
                    <th>Категорија</th>
                    <th>Поени</th>
                    <th>Опције</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($pitanja)){
                 foreach($pitanja as $pitanja){
                  print '<tr>';
                    print '<td>'.$pitanja['pitanje_text'].'</td>';
                    print '<td>'.$pitanja['kategorija'].'</td>';
                    print '<td>'.$pitanja['pitanje_poeni'].'</td>';
                    print '<td>';
                        print anchor('adminpanel/index/izmeni/'.$pitanja['id_pitanje'],'Измени');
                        print '&nbsp&nbsp';
                        print anchor('adminpanel/index/obrisi/'.$pitanja['id_pitanje'],'Обриши');
                    print '</td>';
                  print '</tr>';
                 }
                }
            ?>
        </tbody>
    </table>
		
</div>

