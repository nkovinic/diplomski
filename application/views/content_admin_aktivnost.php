<div class="container">
    

    <div class="col-lg-6 col-md-12 col-sm-8">
        <h3>Претрага активности:</h3>
        <input type="text" ng-model="aktivnosti" class="form-control textarea-autosize"/>
        <br/>
    </div>
    <table class="table table-hover table-stripe"  title="Tabela pitanja" ng-controller="aktivnosti">
        <thead>
            <tr>
                <th>Емаил</th>
                <th>Број поена</th>
                <th>Датум рађења теста</th>
                <th>Опције</th>
            </tr>
        </thead>
        <tbody >
            <tr ng-repeat=" a in a.pitanja | filter: aktivnosti | limitTo: 50" >
                <td>{{ a.email_aktivnost }}</td>
                <td>{{ a.poeni_aktivnost }}</td>
                <td>{{ a.datum_aktivnost }}</td>
                <td>
                    <a href="aktivnosti/obrisi/{{ a.id_aktivnost }}">Обриши</a>
                </td>
            </tr>
            
        </tbody>
    </table>
	
    
</div>