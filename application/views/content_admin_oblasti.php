<div class="container">
    
    
    <div class="col-lg-6 col-md-12 col-sm-8">
        <?php
            print form_open($formm);
            print form_label('Za izmenu/dodavanje oblasti','float-textarea-autosize',$lbIzmen);
            print form_input($taIzmena);
            print "<br/>";
            print form_button($btnIzmena);
            print form_button($btnDodaj);
            print form_close();
        ?>
    </div>
    <table class="table table-hover table-stripe" title="Tabela članova">
        <thead>
            <tr>
                    <th>Naziv Oblasti</th>
                    <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($oblasti)){
                 foreach($oblasti as $oblasti){
                  print '<tr>';
                    print '<td>'.$oblasti['oblast_text'].'</td>';
                    print '<td>';
                        print anchor('adminpanel/Oblasti/izmeni/'.$oblasti['id_oblasti'],'Izmeni');
                        print '&nbsp&nbsp';
                        print anchor('adminpanel/Oblasti/obrisi/'.$oblasti['id_oblasti'],'Obrisi');
                    print '</td>';
                  print '</tr>';
                 }
                }
            ?>
        </tbody>
    </table>
		
</div>