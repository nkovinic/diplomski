<div class="container">
    

    <div class="col-lg-6 col-md-12 col-sm-8">
        <h3>Претрага питања:</h3>
        <input type="text" ng-model="pretraga" class="form-control textarea-autosize"/>
        <br/>
    </div>
    <table class="table table-hover table-stripe"  title="Tabela pitanja" ng-controller="pretraga">
        <thead>
            <tr>
                    <th>Текст питања</th>
                    <th>Категорија</th>
                    <th>Поени</th>
                    <th>Опције</th>
            </tr>
        </thead>
        <tbody >
            <tr ng-repeat=" p in a.pitanja | filter: pretraga | limitTo: 50" >
                <td>{{ p.pitanje_text }}</td>
                <td>{{ p.kategorija }}</td>
                <td>{{ p.pitanje_poeni }}</td>
                <td>
                    <a href="izmeni/{{ p.id_pitanje }}">Измени</a>
                    <a href="obrisi/{{ p.id_pitanje }}">Обриши</a>
                </td>
            </tr>
            
        </tbody>
    </table>
	
    
</div>