<div class="container">
                    <h1 class="heading">Logovanje</h1>

         <?php   
          $validacija=$this->session->flashdata('validacija');
          if(!empty($validacija)){  echo "<div class='alert alert-danger'>".$validacija."</div>";}
          $uspeh=$this->session->flashdata('ulogovao');
          if(!empty($uspeh))    {  echo "<div class='alert alert-success'>".$uspeh."</div>"; }?>
          <?php   echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
          <?php print form_open('adminlogovanje/');?>
                <fieldset>
                    <!--<legend>Logovanje</legend> -->
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Email</label>
                                <?php print form_input($Email)?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Sifra</label>
                                <?php print form_input($Sifra)?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-btn">
                        <div class="row">
                            <div class="col-lg-4 col-lg-push-2 col-md-6 col-md-push-3 col-sm-8 col-sm-push-4">
                                <?php print form_button($Logovanje); ?>
                                <?php print form_button($Ponisti); ?>				
                            </div>
                        </div>
                    </div>
                </fieldset>
            <?php print form_close();?>
        </div>
		
</div>