
        <div class="navbar navbar-inverse navbar-fixed-bottom" style="text-align: center">
            
            <p style="color: white;font-size: 16px;padding-top: 12px;padding-left: 36px;">НАПОМЕНА: ово су тестови намењени искључиво за проверу свог знања и не користе се у комерцијалне сврхе. Copyright <a href="www.nbn.pe.hu">NBN technology</a> 2016.</p>
        </div>
        <!-- /.container -->
    
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php print base_url(); ?>js/jquery-1.10.1.min.js"></script>
    <script src="<?php print base_url(); ?>js/sweetalert.min.js"></script>
	
    <link rel="stylesheet" type="text/css" href="<?php print base_url(); ?>css/sweetalert.css">  
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php print base_url(); ?>js/bootstrap.min.js"></script>
    
    <script>			
        var app = angular.module('myApp', []);			
        app.controller('pretraga', function($scope, $http) {
        $http.get("json").then(function(response) {
                $scope.a = response.data;
            });
        });
        
        
        
        app.controller('aktivnosti', function($scope, $http) {
        $http.get("json_aktivnosti").then(function(response) {
                $scope.a = response.data;
            });
        });
        
        var kat = <?php print $kat?>;
        app.controller('pitanja', function($scope, $http) {
        $http.get("pitanja/" + kat).then(function(response) {
                $scope.podaci = response.data;

                $scope.getSum = function(){
                        var brBodova = 0;
                        for(var i = 0; i < $scope.podaci.pitanja.length; i++){
                                var pitanja = $scope.podaci.pitanja[i];
                                brBodova = brBodova + JSON.parse(pitanja.pitanje_poeni);
                        }
                        return brBodova;
                }

            });
        
        
        
        var brBodova1 = 0;
	
        
        $scope.rbCheck = function(tacan,name,idO,b,o){
					
                if(tacan == 1){
                        var radios = document.getElementsByName(name);
                        for (var i = 0; i< radios.length;  i++){
                                radios[i].disabled = true;

                                var a = angular.element(radios[i]).data('val');
                                
                                var idOdg = angular.element(radios[i]).data('idodg');
                                if(a == 1){							
                                        $("label[for = '" + idOdg +"']").css('color','green');						
                                }else{
                                        $("label[for = '" + idOdg +"']").css('color','red');	
                                }

                        }
                        brBodova = brBodova + parseInt(b);
                        alert(brBodova);
                }else if(tacan == 0){
                        var radios = document.getElementsByName(name);
                        for (var i = 0; i< radios.length;  i++){
                                radios[i].disabled = true;

                                var a = angular.element(radios[i]).data('val');
                                var idOdg = angular.element(radios[i]).data('idodg');
                                if(a == 1){							
                                        $("label[for = '" + idOdg +"']").css('color','green');						
                                }else{
                                        $("label[for = '" + idOdg +"']").css('color','red');	
                                }

                        }
                }
        }
        
            $scope.chbChecked = function(o,p){


                    if(o.odgovor_tacan == 1) {
                            $("label[for = '" + o.id_odgovor +"']").css('color','green');						
                    }else if(o.odgovor_tacan == 0){
                            $("label[for = '" + o.id_odgovor +"']").css('color','red');
                    }


            };

            
          
        });
    </script>
    
    
</body>

</html>
