<!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="izaberi">
                    <?php print form_open('start/kategorija')?>
                        <h2>Изаберите жељену категорију</h2>
                        <select name="ddlKategorije" id="ddlKategorije">
                            <option value="0">Изабери...</option>
                            <?php foreach($kategorije as $k):?>
                            <option value="<?php print $k->id_kategorija ?>"><?php print $k->kategorija?></option>
                            <?php endforeach; ?>
                        </select><br/>
                        <button class="btnStart" id="btnStart" name="btnStart">Започни тест</button>
                    </form>
                    <p>Број тачних одговора за пролаз на испиту мора да прелази <b>85%</b> од укупног броја бодова, испит траје 60мин.</p>
                </div>
            </div>           
        </div>
    </div>