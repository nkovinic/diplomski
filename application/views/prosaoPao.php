<!-- Page Content -->
    <div class="container">
        <div class="row">
            <section class="col-lg-12">
                
                <div class="<?php $rezultati['procenti'] >= 85 ? print('rezultatProsao') : print('rezultatPao'); ?>">
                    <h2>Испит је завршен!</h2>
                    <?php if($rezultati['procenti'] >= 85):?>
                        <h2>Честитамо, положили сте теоријски испит.</h2>
                    <?php else:?>
                        <h2>Нажалост, нисте положили испит.</h2>
                    <?php endif;?>
                </div>
                <h3>Укупан број бодова на ипиту:&nbsp<?php print $rezultati['ukupnoBodova']; ?>&nbspbodova</h3>
                <h3>Број бодова освојених на испиту:&nbsp<?php print $rezultati['brojBodova']; ?>&nbspboda (<?php print round($rezultati['procenti'],1); ?>%)</h3>
            </section>           
        </div>
    </div>