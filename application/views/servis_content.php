<div class="container" ng-app="myApp" ng-controller="pitanja">
	<div class="pitanja" ng-repeat="p in podaci.pitanja" >
		<div class="pContent">
			<h2 data-vrsta="{{ p.pitanje_vrsta }}">{{ p.pitanje_text }} ({{ p.pitanje_poeni }} boda)</h2>
			<span ng-if=" p.pitanje_slika != null || p.pitanje_slika != ''">
				<img class="img-responsive" src="<?php print base_url();?> {{ p.pitanje_slika }}" /><br/>
			</span>
			<span ng-repeat="odg in podaci.odgovori">
				<span ng-repeat="o in odg" ng-if="o.id_pitanje == p.id_pitanje">
					<span ng-if="p.pitanje_vrsta == 1">
						<input id="{{ o.id_odgovor }}"  name="{{ 'chb' + p.id_pitanje }}" type="checkbox"  ng-click="chbChecked(o,p)" /><label for="{{ o.id_odgovor }}">{{ o.odgovor_text }}</label><br/>
					</span>
					<span ng-if="p.pitanje_vrsta == 0">
						<input id="{{ o.id_odgovor }}"  type="radio" class="rb{{ p.id_pitanje }}" data-idodg="{{ o.id_odgovor }}"  data-val="{{ o.odgovor_tacan }}"  name="rb{{ p.id_pitanje }}" ng-click="rbCheck(o.odgovor_tacan,'rb' + p.id_pitanje,o.id_odgovor,p.pitanje_poeni)" /><label for="{{ o.id_odgovor }}">{{ o.odgovor_text }}</label><br/>
					</span>
				</span>
			</span>                            
		</div>    
	</div>
    <div class="pFooter">
        <button type="button" id="btnZavrsi" class="btnZavrsi">Заврши тест</button>                    
	</div>                       
</div>
	
   