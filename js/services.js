
	
	var app = angular.module('myApp', []);
	app.controller('pitanja', function($scope, $http) {
		var brBodova = 0;

		$scope.chbChecked = function(o,p){
			if(o.odgovor_tacan == 1) {
				$("label[for = '" + o.id_odgovor +"']").css('color','green');						
			}else if(o.odgovor_tacan == 0){
				$("label[for = '" + o.id_odgovor +"']").css('color','red');
			}	
		}
		
		$scope.rbCheck = function(tacan,name,idO,b){
			
			if(tacan == 1){
				var radios = document.getElementsByName(name);
				for (var i = 0; i< radios.length;  i++){
					radios[i].disabled = true;
					
					var a = angular.element(radios[i]).data('val');
					var idOdg = angular.element(radios[i]).data('idodg');
					if(a == 1){							
						$("label[for = '" + idOdg +"']").css('color','green');						
					}else{
						$("label[for = '" + idOdg +"']").css('color','red');	
					}
				
				}
				brBodova = brBodova + parseInt(b);
				alert(brBodova);
			}else if(tacan == 0){
				var radios = document.getElementsByName(name);
				for (var i = 0; i< radios.length;  i++){
					radios[i].disabled = true;
					
					var a = angular.element(radios[i]).data('val');
					var idOdg = angular.element(radios[i]).data('idodg');
					if(a == 1){							
						$("label[for = '" + idOdg +"']").css('color','green');						
					}else{
						$("label[for = '" + idOdg +"']").css('color','red');	
					}
				
				}
			}
		}
            
		
		$scope.izberi = function(kategorija){
			$http.get("servicesStart/pitanja/" + kategorija).then(function(response) {
				$scope.podaci = response.data;
				
				$scope.getSum = function(){
					var brBodova = 0;
					for(var i = 0; i < $scope.podaci.pitanja.length; i++){
						var pitanja = $scope.podaci.pitanja[i];
						brBodova = brBodova + JSON.parse(pitanja.pitanje_poeni);
					}
					return brBodova;
				}
									
			});
		};
	});			
        app.controller('pretraga', function($scope, $http) {
        $http.get("json").then(function(response) {
                $scope.a = response.data;
            });
        });
        
        app.controller('aktivnosti', function($scope, $http) {
        $http.get("json_aktivnosti").then(function(response) {
                $scope.a = response.data;
            });
        });

	app.controller('izaberi', function($scope, $http) {
        $http.get("json").then(function(response) {
                $scope.kategorije = response.data;
            });
        });
									
